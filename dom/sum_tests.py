# -*- coding: utf-8 -*-

import unittest
from sum_client import client


class SumTestCase(unittest.TestCase):

    def test_sum(self):
        message = '10 12'

        actual = client(message)
        expected = '22.0'

        self.assertEqual(expected, actual)

    def test_sum_error1(self):
        message = 'ada'

        actual = client(message)
        expected = "Niepoprawne dane"

        self.assertEqual(expected, actual)
if __name__ == '__main__':
    unittest.main()