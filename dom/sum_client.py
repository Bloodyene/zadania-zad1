# -*- coding: utf-8 -*-

import socket


def client(message):
    server_address = ('194.29.175.240', 9193)
    s = socket.socket()
    print 'nawiązuję połączenie z {0} na porcie {1}'.format(*server_address)
    s.connect(server_address)
    try:
        # Wysłanie danych
        print 'wysyłam {0}'.format(message)
        s.sendall(message)
        # Odebranie odpowiedzi
        buffsize = 4096
        response = ''
        done = False
        while not done:
            part = s.recv(buffsize)
            if len(part) < buffsize:
                done = True
            response += part


        print 'odebrano "{0}"'.format(response.decode('utf-8'))

    finally:
        # Zamknięcie połączenia na zakończenie działania

        s.close()
        print 'połączenie zostało zakończone'
    return response
if __name__ == '__main__':
    liczba1 = raw_input(u'Wprowadź pierwszys skladnik sumy: ')
    liczba2 = raw_input(u'Wprowadź drugi skladnik sumy: ')
    client('{0} {1}'.format(liczba1, liczba2))
