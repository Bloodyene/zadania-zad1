# -*- coding: utf-8 -*-


import sys

import socket


def server():
    server_address = ('194.29.175.240', 9193)
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind(server_address)
    print 'tworzę serwer na {0}:{1}'.format(*server_address)
    s.listen(1)
    #suma = 0.0
    try:
        while True:
            print 'czekam na połączenie'
            conn, addr= s.accept()
            print 'połączono z {0}:{1}'.format(*addr)
            try:
            # Odebranie danych
                buffsize = 4096
                data = ""
                while True:
                    part = conn.recv(buffsize)
                    data += part
                    if len(part) < buffsize: break
                print 'otrzymano "{0}"'.format(data)
                data = data.split(" ")
                if len(data) > 2: raise
                result = float(data[0]) + float(data[1])
                conn.sendall(str(result))
                print 'odesłano sume liczb do klienta'
            except:
                conn.sendall("Niepoprawne dane")
            finally:
                # Zamknięcie połączenia
                conn.close()
                print 'zamknięto połączenie'
                print 'otrzymano "{0}"'.format(data)
                # Odesłanie odebranych danych spowrotem

    except KeyboardInterrupt:
        s.close()
if __name__ == '__main__':

    server()
    sys.exit(0)